$(document).ready(function(){
	$('.languages').find('.click_block').click(function(e){
		$(this).toggleClass('active');
		$(this).next().toggle();
        e.preventDefault();
	});
	$('.hidden_menu').hide();
	$('.show_menu').click(function(){
		$(this).next().fadeIn(300);
	});
	$('.hidden_menu .top').find('.icon_mdl').click(function(){
		$(this).parents('.hidden_menu').fadeOut(300);
	});
	$('.nav_popup').hide();
	$('.hidden_menu').find('.opens_popup.change').next().slideDown();
	$('.hidden_menu').find('.opens_popup').click(function(){
		$(this).toggleClass('change').next().slideToggle();
	});
	// $('.hidden_menu').find('.opens_popup_second').click(function(){
	// 	$(this).toggleClass('change').next().slideToggle();
	// });
    $('ul.tabs').delegate('li:not(.current)', 'click', function() {
        $(this).addClass('current').siblings().removeClass('current')
            .parents('div.tabs_section').find('div.tab_info').hide().eq($(this).index()).fadeIn(150);
    });
    $('ul.tabs_low').delegate('li:not(.current)', 'click', function() {
        $(this).addClass('current').siblings().removeClass('current')
            .parents('div.section_low').find('div.box').hide().eq($(this).index()).fadeIn(150);
    });
    $('.upstair').click(function() {
	    $('html, body').animate({ scrollTop:0 }, 'slow');
	    return false;
	});

// Number function
    $('#number .minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('#number .plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
// Number function:end
	// $('.tab_accordion,.search_accordion').find('.hidden_list').hide();
	// $('.tab_accordion,.search_accordion').find('h2').click(function(){
	// 	$(this).toggleClass('rotate').next().slideToggle();
	// });

    $('.popup').find('.close').click(function(){
                $('.popup').fadeOut(600);
            });
            $('.popup').click(function(){
                $(this).fadeOut(600);
            });
            $('.popup-content').click(function(e){
                e.stopPropagation();
            });
    $('.partner_program .description').hide();
    $('.pluses_partnership .name').find('a').click(function(e){
        e.preventDefault();
        $(this).parents('.pluses_partnership').next().slideToggle();
    })






    $('a.change_pass,.secret_question').click(function(e){
        e.preventDefault();
    })
    $('.security_settings .change_popup_wrap,.security_settings .message_popup_wrap').find('a').click(function(e){
        e.preventDefault();
    })
    $('.change_popup_wrap,.message_popup_wrap').hide()
    $('a.change_pass,.profile_accordion a.change_pass').click(function(){
        $('.change_popup_wrap.close_question_popup').hide();
        $('.change_popup_wrap.popup_pass1').show();
    })
    $('.change_popup_wrap.popup_pass1').find('a.back').click(function(){
        $('.change_popup_wrap.popup_pass1').hide();
    })
    $('.change_popup_wrap.popup_pass1').find('a.continue').click(function(){
        $('.change_popup_wrap.popup_pass1').hide();
        $('.change_popup_wrap.popup_pass2').show();
    })
    $('.change_popup_wrap.popup_pass2').find('a.back').click(function(){
        $('.change_popup_wrap.popup_pass2').hide();
        $('.change_popup_wrap.popup_pass1').show();
    })
    $('.change_popup_wrap.popup_pass2').find('a.continue').click(function(){
        $('.change_popup_wrap.popup_pass2').hide();
        $('.change_popup_wrap.popup_pass3').show();
    })
    $('.change_popup_wrap.popup_pass3').find('a.back').click(function(){
        $('.change_popup_wrap.popup_pass3').hide();
        $('.change_popup_wrap.popup_pass2').show();
    })
    $('.security_settings .change_popup_wrap.popup_pass3').find('a.save').click(function(){
        $('.change_popup_wrap.popup_pass3').hide();
        $('.message_popup_wrap.message_popup_pass').show();
    })

    $('.secret_question_wrap').find('a.secret_question').click(function(){
        $('.change_popup_wrap.question1').show();
        $('.change_popup_wrap.close_pass_popup').hide();
    })
    $('.change_popup_wrap.question1').find('a.back').click(function(){
        $('.change_popup_wrap.question1').hide();
    })
    $('.change_popup_wrap.question1').find('a.continue').click(function(){
        $('.change_popup_wrap.question1').hide();
        $('.change_popup_wrap.question2').show();
    })
    $('.change_popup_wrap.question2').find('a.back').click(function(){
        $('.change_popup_wrap.question2').hide();
        $('.change_popup_wrap.question1').show();
    })
    $('.change_popup_wrap.question2').find('a.continue').click(function(){
        $('.change_popup_wrap.question2').hide();
        $('.change_popup_wrap.question3').show();
    })
    $('.change_popup_wrap.question3').find('a.back').click(function(){
        $('.change_popup_wrap.question3').hide();
        $('.change_popup_wrap.question2').show();
    })
    $('.change_popup_wrap.question3').find('a.save').click(function(){
        $('.change_popup_wrap.question3').hide();
        $('.message_popup_wrap.message_popup_question').show();
    })
    $('.message_popup_wrap .close').click(function(){
        $('.message_popup_wrap').hide();
    })



});

function PopUpShowStat(){
    $("#stat_popup").fadeIn(600);
};
